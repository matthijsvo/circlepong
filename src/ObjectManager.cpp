#include "stdafx.h"
#include "ObjectManager.h"
#include "Game.h"

ObjectManager::ObjectManager()
{

}

ObjectManager::~ObjectManager()
{
	std::for_each(_gameObjects.begin(), _gameObjects.end(), GameObjectDeallocator());
}

void ObjectManager::add(std::string name, VisibleObject* gameObject)
{
	_gameObjects.insert(std::pair<std::string, VisibleObject*>(name, gameObject));
}

void ObjectManager::remove(std::string name)
{
	std::map<std::string, VisibleObject*>::iterator results = _gameObjects.find(name);
	if(results != _gameObjects.end() )
	{
		delete results->second;
		_gameObjects.erase(results);
	}
}

VisibleObject* ObjectManager::get(std::string name) const
{
	std::map<std::string, VisibleObject*>::const_iterator results = _gameObjects.find(name);
	if(results == _gameObjects.end() )
	{
		return NULL;
	}
	return results->second;
}

int ObjectManager::getObjectCount() const
{
	return _gameObjects.size();
}

void ObjectManager::drawAll(sf::RenderWindow& renderWindow)
{
	std::map<std::string, VisibleObject*>::const_iterator it = _gameObjects.begin();
	while(it != _gameObjects.end())
	{
		it->second->draw(renderWindow);
		++it;
	}
}

void ObjectManager::updateAll()
{
	std::map<std::string,VisibleObject*>::const_iterator it = _gameObjects.begin();
	float timeDelta = _clock.restart().asSeconds();

	while(it != _gameObjects.end())
	{
		it->second->update(timeDelta);
		++it;
	}
}