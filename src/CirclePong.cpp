// CirclePong.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "CirclePongGame.h"

int main()
{
	//1024x768 works best at the moment, though all (reasonable) combinations should be possible.
	CirclePongGame game(1024, 768);
	game.start();

	return 0;
}

