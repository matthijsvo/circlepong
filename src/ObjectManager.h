#pragma once
#include "VisibleObject.h"

class ObjectManager
{
public:
	ObjectManager(void);
	~ObjectManager();

	void			add(std::string name, VisibleObject* gameObject);
	void			remove(std::string name);
	int				getObjectCount() const;
	VisibleObject*	get(std::string name) const;

	void			drawAll(sf::RenderWindow& renderWindow);
	void			updateAll();

private:
	std::map<std::string, VisibleObject*> _gameObjects;

	sf::Clock		_clock;

	struct			GameObjectDeallocator
	{
		void operator() (const std::pair<std::string, VisibleObject*>& p) const
		{
			delete p.second;
		}
	};
};