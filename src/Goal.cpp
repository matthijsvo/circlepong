#include "stdafx.h"
#include "Goal.h"

Goal::Goal(float radius, int value) : 
	Arena(radius, value), _score(-1)
{
	sf::CircleShape circle(radius/2,100);
	circle.setOutlineThickness(10);

	circle.setFillColor(sf::Color(230,230,230,190));

	circle.setOutlineColor(sf::Color(value, value, value));
	circle.setOrigin(radius/2,radius/2);

	setShape(circle);

	_particles = new ParticleSystem(10000, 20);
	_particles->setEmitter(getPosition());

	if(!_font.loadFromFile("assets/fonts/FuturaLT.ttf"))
	{
		std::cout<<"ERROR: FONT NOT FOUND"<<std::endl;
	}
	else
	{
		_scoreText.setFont(_font);
		_scoreText.setCharacterSize(60);
		_scoreText.setStyle(sf::Text::Regular);
		_scoreText.setColor(sf::Color(150,150,150));
		_scoreText.setString(int2Str(_score));
		_scoreText.setPosition(getPosition());
	}
}

Goal::~Goal()
{
	//nope
}

void Goal::upScore()
{
	if(_score<999) ++_score;
}

void Goal::changeColor(sf::Color newColor)
{
	_shape.setFillColor(newColor);
}

void Goal::update(float elapsedTime)
{
	sf::Time elapsed = _clock.restart();
	_particles->update(elapsed);
	_particles->setEmitter(getPosition());

	size_t characterSize = _scoreText.getCharacterSize();
	std::string string = _scoreText.getString().toAnsiString();
	size_t maxHeight = 0;

	for (size_t x=0; x<_scoreText.getString().getSize(); ++x)
	{
		sf::Uint32 character = string.at(x);

		const sf::Glyph& currentGlyph = _font.getGlyph(character, characterSize, false);

		size_t height = currentGlyph.bounds.height;

		if (maxHeight < height)
			maxHeight = height;
	}

	sf::FloatRect rect = _scoreText.getGlobalBounds();

	rect.left = 22; 
	rect.top = getPosition().y - (maxHeight/2.0f) - (rect.height-characterSize)/2.0f - 20;

	_scoreText.setPosition(rect.left, rect.top);
	_scoreText.setString(int2Str(_score));
}

void Goal::draw(sf::RenderWindow& window)
{
	window.draw(_shape);
	window.draw(*_particles);
	window.draw(_scoreText);
}