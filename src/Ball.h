#pragma once
#include "VisibleObject.h"
#include "ObjectManager.h"
#include "ParticleSystem.h"

class Ball : public VisibleObject
{
public:
	Ball(float radius, ObjectManager* objectManager);
	~Ball();

	void			setObjectManager(ObjectManager* objectManager);

	void			draw(sf::RenderWindow& window);
	void			update(float elapsedTime);
	
	const int		getRadius();

private:
	float			_radius;
	ObjectManager*	_objectManager;
	ParticleSystem*	_particles;
	sf::Clock		_clock;

	bool			_inGoal;

	float			_velocity;
	double			_angle;
	float			_elapsedTimeSinceStart;

private:
	float			LinearVelocityX(float angle);
	float			LinearVelocityY(float angle);
};