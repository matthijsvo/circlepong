#pragma once
#include "VisibleObject.h"

class Arena : public VisibleObject
{
public:
	Arena(float radius, int value, bool transparent = false);
	~Arena();
	const int		getRadius();

private:
	float			_radius;
};