#include "stdafx.h"
#include "Game.h"

Game::Game(int sw, int sh) : SCREEN_WIDTH(sw), SCREEN_HEIGHT(sh), _gameState(Uninitialized)
{

}

Game::~Game()
{

}

bool Game::isExiting()
{
	return _gameState == Exiting;
}

sf::RenderWindow& Game::getWindow()
{
	return _mainWindow;
}

const ObjectManager& Game::getObjectManager()
{
	return _objectManager;
}