#include "stdafx.h"
#include "CirclePongGame.h"
#include "Arena.h"
#include "Goal.h"
#include "Ball.h"
#include "PlayerPaddle.h"

CirclePongGame::CirclePongGame(int sw, int sh) : 
	Game(sw,sh)
{
	//nope
}

CirclePongGame::~CirclePongGame()
{
	//nope
}

void CirclePongGame::start()
{
	if(_gameState != Uninitialized) return; // Something went terribly wrong...

	//Set up rendering
	sf::ContextSettings AA(0,0,8,2,0); // Anti-Alias x8
	_mainWindow.create(sf::VideoMode(SCREEN_WIDTH,SCREEN_HEIGHT,32),"CIRCLE PONG", sf::Style::Default, AA);
	_mainWindow.setFramerateLimit(60);

	//Set up view (for rotation)
	_mainView = sf::View(sf::Vector2f((SCREEN_WIDTH/2), (SCREEN_HEIGHT/2)), sf::Vector2f((SCREEN_WIDTH), (SCREEN_HEIGHT)));
	_mainWindow.setView(_mainView);

	_gameState = Playing;

	//Initiate objects
	Arena* arena = new Arena(SCREEN_HEIGHT-20,50);
	arena->setPosition((SCREEN_WIDTH/2), (SCREEN_HEIGHT/2));
	_objectManager.add("arena", arena);

	Goal* center = new Goal(SCREEN_WIDTH/5,150);
	center->setPosition((SCREEN_WIDTH/2), (SCREEN_HEIGHT/2));
	_objectManager.add("center", center);

	Ball* ball = new Ball(SCREEN_WIDTH/40, &_objectManager);
	ball->setPosition((SCREEN_WIDTH/2), (SCREEN_HEIGHT/2)+40);
	_objectManager.add("ball", ball);

	PlayerPaddle* player1 = new PlayerPaddle((SCREEN_WIDTH/35), sf::Vector2i((SCREEN_WIDTH/2),(SCREEN_HEIGHT/2)), &_objectManager);
	player1->setPosition((SCREEN_WIDTH/2), (SCREEN_HEIGHT/2)+SCREEN_WIDTH/5+20);
	_objectManager.add("player1", player1);

	//Main loop
	while(_mainWindow.isOpen())
	{
		gameLoop();
	}

	_mainWindow.close();
}

void CirclePongGame::gameLoop()
{
	sf::Event event;

	//Catch pause command. Buggy as hell
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		if(_gameState != Paused) 
		{
			_gameState = Paused;
		}
		else _gameState = Playing;
	}

	switch(_gameState)
	{
	case Paused:
		while(_mainWindow.pollEvent(event))
		{
			switch(event.type)
			{
			case sf::Event::Closed:
				_mainWindow.close();
				break;
			}
		}
		break;
	case Playing:
		while(_mainWindow.pollEvent(event))
		{
			switch(event.type)
			{
			case sf::Event::Closed:
				_mainWindow.close();
				break;
			}
		}
		_objectManager.updateAll(); // Update positions of all objects
		_mainView.rotate(0.5f);
		_mainWindow.setView(_mainView);
		break;
	}
	//Main draw
	_mainWindow.clear(sf::Color(0,0,0));
	_objectManager.drawAll(_mainWindow);

	//Display all
	_mainWindow.display();
}
