#pragma once
#include "Game.h"
#include "ParticleSystem.h"

class CirclePongGame : public Game
{
public:
	CirclePongGame(int sw, int sh);
	~CirclePongGame();

	void			start();

protected:
	void			gameLoop();

private:
	ParticleSystem* particles;
	sf::Clock		clock;
	sf::View		_mainView;
};
