#pragma once
#include "VisibleObject.h"
#include "ObjectManager.h"

class PlayerPaddle :
	public VisibleObject
{
public:
	PlayerPaddle(float radius, sf::Vector2i center, ObjectManager* objectManager);
	~PlayerPaddle();

	float			getRadius();

	void			update(float elapsedTime);
	void			draw(sf::RenderWindow& rw);
private:
	float			_radius;
	float			_radius2;
	float			_radius2max;
	float			_radius2min;
	ObjectManager*	_objectManager;

	sf::Vector2i	_center;
	float			_currentAngle;

};