#pragma once

#include <sstream>
#include "Arena.h"
#include "ParticleSystem.h"

class Goal : public Arena
{
public:
	Goal(float radius, int value);
	~Goal();

	void			draw(sf::RenderWindow& window);
	void			update(float elapsedTime);

	void			upScore();
	void			changeColor(sf::Color newColor);

private:
	double			_score;
	sf::Font		_font;
	sf::Text		_scoreText;

	ParticleSystem*	_particles;
	sf::Clock		_clock;

	static inline std::string int2Str(int x)
	{
		std::stringstream type;
		type << x;
		return type.str();
	}
};