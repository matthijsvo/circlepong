#include "stdafx.h"
#include "PlayerPaddle.h"
#include "Arena.h"

PlayerPaddle::PlayerPaddle(float radius, sf::Vector2i center, ObjectManager* objectManager) : 
	_radius(radius/2-6), 
	_currentAngle(270.0f), 
	_center(center), 
	_objectManager(objectManager)
{
	sf::CircleShape circle(radius/2,30);
	circle.setOutlineThickness(6);
	circle.setFillColor(sf::Color::Transparent);
	circle.setOutlineColor(sf::Color(180, 180, 180));
	circle.setOrigin(radius/2,radius/2);

	setShape(circle);

	Arena* center2 = dynamic_cast<Arena*>(_objectManager->get("center"));
	_radius2 = center2->getRadius() + 100;
	_radius2max = center2->getRadius() + 200;
	_radius2min = center2->getRadius() + 40;
}
PlayerPaddle::~PlayerPaddle()
{
	//nope
}

void PlayerPaddle::draw(sf::RenderWindow & rw)
{
	VisibleObject::draw(rw);
}

float PlayerPaddle::getRadius()
{
	return _radius;
}

void PlayerPaddle::update(float elapsedTime)
{
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		if(_currentAngle == 0.0f || _currentAngle == -2.0f) _currentAngle = 360.0f;
		if(_currentAngle > 0.0f) _currentAngle -= 4.0f; //FIXME put on 180.0f
	}
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		if(_currentAngle == 360.0f || _currentAngle == 362.0f) _currentAngle = 0.0f;
		if(_currentAngle < 360.0f) _currentAngle += 4.0f;
	}

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		if(_radius2 <= _radius2max) _radius2 += 4.0f;
	}
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		if(_radius2 >= _radius2min) _radius2 -= 4.0f;
	}

	float newX = _radius2 * cos(_currentAngle * ( 3.14159265 / 180.0 )) + _center.x;
	float newY = _radius2 * sin(_currentAngle * ( 3.14159265 / 180.0 )) + _center.y;

	_shape.setPosition(newX, newY);
}