#pragma once
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "ObjectManager.h"

class Game
{
public:
	Game(int sw, int sh);
	virtual ~Game();
	virtual void start() =0;
	sf::RenderWindow& getWindow();
	const sf::Event& getInput();
	const ObjectManager& getObjectManager();

protected:
	bool isExiting();
	virtual void gameLoop() =0;

	const int SCREEN_WIDTH;
	const int SCREEN_HEIGHT;

	enum GameState { Uninitialized, ShowingSplash, Paused, 
		ShowingMenu, Playing, Exiting };

	GameState _gameState;
	sf::RenderWindow _mainWindow;

	ObjectManager _objectManager;
};