#include "stdafx.h"
#include "VisibleObject.h"

VisibleObject::VisibleObject() : 
	_isLoaded(false)
{

}

VisibleObject::~VisibleObject()
{

}

void VisibleObject::setShape(sf::CircleShape shape)
{
	_shape = shape;
	_isLoaded = true;
}

const sf::CircleShape& VisibleObject::getShape()
{
	return _shape;
}

void VisibleObject::draw(sf::RenderWindow& window)
{
	if(_isLoaded)
		window.draw(_shape);
}

void VisibleObject::update(float elapsedTime)
{

}

void VisibleObject::setPosition(float x, float y)
{
	if(_isLoaded)
		_shape.setPosition(x,y);
}

sf::Vector2f VisibleObject::getPosition() const
{
	if(_isLoaded)
		return _shape.getPosition();
	else
		return sf::Vector2f();
}

float VisibleObject::distanceTo(const VisibleObject* other)
{
	sf::Vector2f pos1 = _shape.getPosition();
	sf::Vector2f pos2 = other->getPosition();

	return sqrt(pow(pos2.x - pos1.x, 2) + pow(pos2.y - pos1.y, 2));
}