#include "stdafx.h"
#include "Arena.h"

Arena::Arena(float radius, int value, bool transparent) : _radius(radius/2 - 10)
{
	sf::CircleShape circle(radius/2,100);
	circle.setOutlineThickness(10);

	if(transparent) circle.setFillColor(sf::Color(230,230,230,170));
	else circle.setFillColor(sf::Color(240,240,240));

	circle.setOutlineColor(sf::Color(value, value, value));
	circle.setOrigin(radius/2,radius/2);
	
	setShape(circle);
}

Arena::~Arena()
{

}

const int Arena::getRadius()
{
	return _radius;
}