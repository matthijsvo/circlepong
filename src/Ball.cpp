#include "stdafx.h"
#include "Ball.h"
#include "Arena.h"
#include "Goal.h"
#include "PlayerPaddle.h"

Ball::Ball(float radius, ObjectManager* objectManager) :
		_radius(radius/2-4), 
		_objectManager(objectManager), 
		_angle(0.0f), 
		_velocity(300.0f), 
		_elapsedTimeSinceStart(0.0f),
		_inGoal(false)
{
	sf::CircleShape circle(radius/2,30);
	circle.setOutlineThickness(4);
	circle.setFillColor(sf::Color::Transparent);
	circle.setOutlineColor(sf::Color(200, 200, 200));
	circle.setOrigin(radius/2,radius/2);

	setShape(circle);

	_particles = new ParticleSystem(20000);
}

Ball::~Ball()
{
	//nope
}

void Ball::setObjectManager(ObjectManager* objectManager)
{
	_objectManager = objectManager;
}

const int Ball::getRadius()
{
	return _radius;
}

void Ball::update(float elapsedTime)
{
	if(_objectManager == NULL) 
	{
		std::cout<<"ERROR: NO OBJECT MANAGER PRESENT"<<std::endl;
		return;
	}
	//Standard movement
	float moveAmount = _velocity * elapsedTime;
	float moveByX = LinearVelocityX(_angle) * moveAmount;
	float moveByY = LinearVelocityY(_angle) * moveAmount;

	//Get objects
	Arena* arena = dynamic_cast<Arena*>(_objectManager->get("arena"));
	Goal* center = dynamic_cast<Goal*>(_objectManager->get("center"));
	PlayerPaddle* player1 = dynamic_cast<PlayerPaddle*>(_objectManager->get("player1"));

	//===================
	//COLLISION DETECTION
	//===================
	//.Arena
	float arenaOverlap = distanceTo(arena) - arena->getRadius();
	if(arenaOverlap >= 0)
	{
		//get positions
		sf::Vector2f ballPos = _shape.getPosition();
		sf::Vector2f arenaPos = arena->getPosition();

		//calculate angles
		float beta = atan2f(ballPos.y-arenaPos.y, ballPos.x-arenaPos.x) * (180.0f / 3.1415926); //Degree of ball to center
		float delta = _angle - beta;
		float gamma = _angle + delta;

		//place back inside
		float newX = ((arena->getRadius()-3) * cos(beta * ( 3.14159265 / 180.0 ))) + arenaPos.x;
		float newY = ((arena->getRadius()-3) * sin(beta * ( 3.14159265 / 180.0 ))) + arenaPos.y;
		setPosition(newX, newY);

		//set new angle
		_angle = gamma;

		//angle corrections
		while(_angle > 360.0f) _angle -= 360.0f;
		while(_angle < 0) _angle += 360.0f;

		moveByX = LinearVelocityX(_angle) * moveAmount;
		moveByY = LinearVelocityY(_angle) * moveAmount;
	}

	//.Center
	bool inGoal = false;
	if(distanceTo(center) <= getRadius() + center->getRadius())
	{
		center->upScore();
		inGoal = true;
		if(_velocity <= 1000.0f) _velocity += 1.0f;
	}
	if(inGoal != _inGoal)
	{
		if(inGoal) center->changeColor(sf::Color(255,204,51,50));
		else center->changeColor(sf::Color(230,230,230,190));
		_inGoal = inGoal;
	}
	
	//.Player 1
	sf::FloatRect p1BB = player1->getShape().getGlobalBounds();

	if(p1BB.intersects(getShape().getGlobalBounds()))
	{
		//get positions
		sf::Vector2f ballPos = _shape.getPosition();
		sf::Vector2f playerPos = player1->getPosition();

		//calculate angles
		float beta = atan2f(ballPos.y-playerPos.y, ballPos.x-playerPos.x) * (180.0f / 3.1415926); //Degree of ball to player 1 paddle
		float delta = _angle - beta;
		float gamma = _angle + delta;
		_angle = gamma;

		//place back inside
		float newX = ((player1->getRadius()+30) * cos(beta * ( 3.14159265 / 180.0 ))) + playerPos.x;
		float newY = ((player1->getRadius()+30) * sin(beta * ( 3.14159265 / 180.0 ))) + playerPos.y;
		setPosition(newX, newY);

		//angle corrections
		while(_angle > 360.0f) _angle -= 360.0f;
		while(_angle < 0) _angle += 360.0f;

		moveByX = LinearVelocityX(_angle) * moveAmount;
		moveByY = LinearVelocityY(_angle) * moveAmount;
	}
	
	//Final movement
	_shape.move(moveByX,moveByY);

	//Update particles
	sf::Time elapsed = _clock.restart();
	_particles->update(elapsed);
	_particles->setEmitter(getPosition());
	
	_elapsedTimeSinceStart += elapsedTime; //Useless for now.
}

void Ball::draw(sf::RenderWindow& window)
{
	if(_isLoaded)
	{
		window.draw(_shape);
		window.draw(*_particles); // Draw particle trail
	}
}

float Ball::LinearVelocityX(float angle)
{
	return (float)std::cos( angle * ( 3.1415926 / 180.0f ));
}

float Ball::LinearVelocityY(float angle)
{
	return (float)std::sin( angle * ( 3.1415926 / 180.0f ));
}