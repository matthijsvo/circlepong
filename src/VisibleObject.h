#pragma once

class VisibleObject
{
public:
					VisibleObject();
	virtual			~VisibleObject();

	void			setShape(sf::CircleShape shape);
	const sf::CircleShape& getShape();

	virtual	void	draw(sf::RenderWindow& window);
	virtual void	update(float elapsedTime);

	virtual void	setPosition(float x, float y);
	virtual sf::Vector2f getPosition() const;

	float			distanceTo(const VisibleObject* other);

protected:
	sf::CircleShape	_shape;
	bool			_isLoaded;
};